declare namespace Card {
    interface ICard {
        id: number,
        name: string,
        imgSrc: string,
        description: string,
        owner: string,
        link: string
    }
}

export = Card;
