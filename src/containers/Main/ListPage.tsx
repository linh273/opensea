import React, { useState } from 'react'
import { createPortal } from 'react-dom';
import { ICard } from '../../types/card';
import CardItem from './components/CardItem';
import CardModal from './components/CardModal';
import { cardList } from './example';

const bodyElement: HTMLElement = document.body;

const ListPage: React.FC = () => {
    const [selectedCard, setSelectedCard] = useState<ICard>();
    const [showModal, setShowModal] = useState<boolean>(false);

    const onClickCard = (card: ICard) => {
        setSelectedCard(card);
        setShowModal(true);
    }

    return (
        <>
            <div className="flex flex-wrap">
                {cardList.map((item: ICard) => (
                    <CardItem
                        key={item.id}
                        card={item}
                        onClickCard={onClickCard}
                    />
                ))}
            </div>
            {showModal && selectedCard && createPortal(
                <CardModal
                    onClose={() => setShowModal(false)}
                    card={selectedCard}
                />,
                bodyElement
            )}
        </>
    )
}

export default ListPage
