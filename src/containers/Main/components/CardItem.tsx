import React from 'react'
import { ICard } from '../../../types/card';

interface IProps {
    card: ICard,
    onClickCard: (card: ICard) => void
}

const CardItem: React.FC<IProps> = ({
    card,
    onClickCard
}) => {
    return (
        <div
            className="flex flex-col m-4 border-2 overflow-hidden w-60 rounded-md shadow-lg cursor-pointer"
            onClick={() => onClickCard(card)}
        >
            <img
                src={card.imgSrc}
                alt="image"
                className="rounded-t-md hover:scale-110 ease-in duration-500"
            />
            <p className="z-10 p-2 truncate bg-white">
                {card.name}
            </p>
        </div>
    )
}

export default CardItem
