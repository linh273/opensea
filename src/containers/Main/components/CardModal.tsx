import React, { useRef } from 'react'
import useClickOutSide from '../../../hooks/useClickOutSide';
import { ICard } from '../../../types/card';

interface IProps {
    onClose: () => void,
    card: ICard
}

const CardModal: React.FC<IProps> = ({
    onClose,
    card
}) => {
    const ref = useRef<HTMLDivElement>(null);
    useClickOutSide(ref, onClose);

    return (
        <div className="z-20 absolute top-0 right-0 bottom-0 left-0 flex justify-center items-center bg-slate-500 hover:bg-curtain ease-in duration-100">
            <div
                className="relative pb-2 max-w-md m-0-auto bg-white rounded-md"
                ref={ref}
            >
                <button
                    className="absolute top-1 right-1 z-10 w-8 h-8"
                    onClick={onClose}
                >
                    x
                </button>
                <img
                    src={card.imgSrc}
                    alt="Image"
                    className="rounded-t-md"
                />
                <p className="m-2 font-bold">
                    {card.name}
                </p>
                <p className="m-2">
                    {card.description}
                </p>
                <p className="m-2">
                    Owned by <strong>{card.owner}</strong>
                </p>

                <a
                    className="m-2 px-3 py-1 rounded-full bg-sky-600 text-white"
                    href={card.link}
                >
                    Purchase
                </a>
            </div>
        </div>
    )
}

export default CardModal
