import { ICard } from "../../types/card";

export const cardList: ICard[] = [
    {
        id: 0,
        name: "CloneX #12706",
        imgSrc: "https://i.seadn.io/gcs/files/89d197e15082e0f2b05b864459f63f6e.png?auto=format&w=1000",
        description: "20,000 next-gen Avatars, by RTFKT and Takashi Murakami 🌸",
        owner: "bluedreamandreww",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/7423",
    },
    {
        id: 1,
        name: "CloneX #RTFKTCLONEXTM",
        imgSrc: "https://i.seadn.io/gcs/files/30cbed0cee72a5fbc0a348d237f4589f.png?auto=format&w=1000",
        description: "If you own a clone without any Murakami trait please read the terms regarding RTFKT - Owned Content here: https://rtfkt.com/legal-2A",
        owner: "0A9BB3",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/4150",
    },
    {
        id: 2,
        name: "CloneX #12706",
        imgSrc: "https://i.seadn.io/gcs/files/4f59e0e63265fbdd0f33257f6d6078cf.png?auto=format&w=1000",
        description: "20,000 next-gen Avatars, by RTFKT and Takashi Murakami 🌸",
        owner: "393373",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/8423",
    },
    {
        id: 4,
        name: "CloneX #3574",
        imgSrc: "https://i.seadn.io/gcs/files/89d197e15082e0f2b05b864459f63f6e.png?auto=format&w=1000",
        description: "20,000 next-gen Avatars, by RTFKT and Takashi Murakami 🌸",
        owner: "bluedreamandreww",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/7423",
    },
    {
        id: 5,
        name: "CloneX #12706",
        imgSrc: "https://i.seadn.io/gcs/files/c01f24e5cfa71d93b1a2297dbe97258a.png?auto=format&w=1000",
        description: "20,000 next-gen Avatars, by RTFKT and Takashi Murakami 🌸",
        owner: "numb",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/13604",
    },
    {
        id: 6,
        name: "CloneX #3123",
        imgSrc: "https://i.seadn.io/gcs/files/98c0c14ad2c21cb9f52dc0395ed7a1df.png?auto=format&w=3840",
        description: "20,000 next-gen Avatars, by RTFKT and Takashi Murakami 🌸",
        owner: "July",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/2973",
    },
    {
        id: 7,
        name: "CloneX #12706",
        imgSrc: "https://i.seadn.io/gcs/files/89d197e15082e0f2b05b864459f63f6e.png?auto=format&w=1000",
        description: "20,000 next-gen Avatars, by RTFKT and Takashi Murakami 🌸",
        owner: "bluedreamandreww",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/7423",
    },
    {
        id: 8,
        name: "CloneX #RTFKTCLONEXTM",
        imgSrc: "https://i.seadn.io/gcs/files/30cbed0cee72a5fbc0a348d237f4589f.png?auto=format&w=1000",
        description: "If you own a clone without any Murakami trait please read the terms regarding RTFKT - Owned Content here: https://rtfkt.com/legal-2A",
        owner: "0A9BB3",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/4150",
    },
    {
        id: 9,
        name: "CloneX #12706",
        imgSrc: "https://i.seadn.io/gcs/files/4f59e0e63265fbdd0f33257f6d6078cf.png?auto=format&w=1000",
        description: "20,000 next-gen Avatars, by RTFKT and Takashi Murakami 🌸",
        owner: "393373",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/8423",
    },
    {
        id: 10,
        name: "CloneX #12706",
        imgSrc: "https://i.seadn.io/gcs/files/c01f24e5cfa71d93b1a2297dbe97258a.png?auto=format&w=1000",
        description: "20,000 next-gen Avatars, by RTFKT and Takashi Murakami 🌸",
        owner: "numb",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/13604",
    },
    {
        id: 11,
        name: "CloneX #3123",
        imgSrc: "https://i.seadn.io/gcs/files/98c0c14ad2c21cb9f52dc0395ed7a1df.png?auto=format&w=3840",
        description: "20,000 next-gen Avatars, by RTFKT and Takashi Murakami 🌸",
        owner: "July",
        link: "https://opensea.io/assets/ethereum/0x49cf6f5d44e70224e2e23fdcdd2c053f30ada28b/2973",
    }
]