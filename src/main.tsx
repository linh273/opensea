import React from 'react'
import ReactDOM from 'react-dom/client'
import ListPage from './containers/Main/ListPage'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <ListPage />
    </React.StrictMode>,
)
