import { useEffect, RefObject } from "react";

const useClickOutSide = <T extends HTMLElement = HTMLElement>(
    ref: RefObject<T>,
    handler: (event: Event) => void
) => {
    useEffect(() => {
        const listenHandler = (event: MouseEvent) => {
            const el = ref?.current;

            if (!el || el.contains((event.target as Node) || null)) {
                return;
            }

            handler(event);
        };

        document.addEventListener("click", listenHandler, true);

        return () => {
            document.removeEventListener("click", listenHandler, true);
        };
    }, [ref, handler]);
};

export default useClickOutSide;
