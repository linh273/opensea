/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		"./index.html",
		"./src/**/*.{js,ts,jsx,tsx}",
	],
	theme: {
		extend: {
			margin: {
				'0-auto': '0 auto'
			},
			colors: {
				'curtain': '#050404c9'
			}
		},
	},
	plugins: [],
}